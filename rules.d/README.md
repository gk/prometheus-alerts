# Alerting rules

This directory should contain alerting rules. Each file should end
with `.rules` to be parsed by Prometheus. Syntax for alerting rules is
covered by the [upstream documentation][].

[upstream documentation]: https://prometheus.io/docs/prometheus/latest/configuration/alerting_rules/

How the files here are managed is up to the teams, but it might make
sense to keep one file per team, or have a prefix for teams to make
sure there is no naming clashes. Note that the alert `name` field is
global and should be unique per Prometheus cluster.

Note that alerts might not be picked up by Prometheus until the next
restart.

## Example rule

Here's a sample rule, the bridgestrap alerting rule:

    groups:
    - name: bridgestrap
      rules:
      - alert: Bridges down
        expr: bridgestrap_fraction_functional < 0.50
        for: 5m
        labels:
          severity: critical
          team: anti-censorship
        annotations:
          title: Bridges down
          description: Too many bridges down
          summary: Number of functional bridges is `{{$value}}%`
          host: "{{$labels.instance}}"

The key part of the alert is the `expr` setting which is a PromQL
expression that, when evaluated to "true" for more than `5m` (the
`for` settings), will fire an error at the Alertmanager. Also note
the `team` label which will route the message to the right team. Those
routes are defined later, in the `routes` and `receivers` settings.
